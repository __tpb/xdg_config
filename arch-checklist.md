## Installation
- [ ] document initial prep
- [ ] document bootloader
- [ ] document nvidia
- [ ] document nvidia power settings
- [ ] document firefox
- [ ] document tlp
- [ ] document reflector
- [ ] document hypridle
- [ ] document hyprlock
- [ ] document mako


### Nvidia

- `nvidia_drm.modeset=1`
- `nvidia_drm.fbdev=1`
- `nvidia`, `nvidia_modeset`, `nvidia_uvm` and `nvidia_drm` to the initramfs
- mkinitcpio modules
- https://wiki.archlinux.org/title/NVIDIA/Tips_and_tricks
- preserve video memory after suspend

## Packages

### System
- acpid + enable service
- sensors
- reflektor
- tlp
- xdg-user-dirs
- polkit
- efiboomgr
- flatpak
- xfsprogs

### AV
- ffmpeg
- nvidia-dkms
- libva-nvidia-driver
- nvidia-utils
- nvidia-settings
- pipewire
- pipewire-pulse

### Web
- networkmanager
- firefox

### Games
- gamemode
- steam
- gamescope (optional)
- proton-ge (optional)

### DE

- Hyprland
- waybar
- mako
- fuzzel
- kitty
- yazi
- imv
- grim slurp
- zathura
- mvp
- xdg-desktop-portal
- brightnessctl
- hyprpapr/swaybg/swwww
- hyprpicker
- hypridle
- hyprlock
- hyprcursor + theme

### cli
- tar
- curl
- wget
- git
- rg
- fd
- nix
- direnv
- nu

### Fonts
